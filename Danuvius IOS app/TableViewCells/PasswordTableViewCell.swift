//
//  PasswordTableViewCell.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 07.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit

class PasswordTableViewCell: UITableViewCell {
    
    private var businessLogicService = BusinessLogicService()
    
    weak var settingsViewController : SettingsViewController?
    
    func clickPasswordCell(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        DispatchQueue.main.async {
            let passwordViewController = storyBoard.instantiateViewController(withIdentifier: "passwordStoryBoard") as! PasswordViewController
            self.settingsViewController?.navigationController?.pushViewController(passwordViewController, animated: true)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let uiTableViewController: UITableViewController = self.contentView.superview?.parentViewController as! UITableViewController
        self.settingsViewController = (uiTableViewController.view.superview?.parentViewController as! SettingsViewController)
        if selected {
            switch self.textLabel?.text {
            case "Passwort ändern":
                clickPasswordCell()
            case "User Logout":
                businessLogicService.userLogOut(navigationController: (self.settingsViewController?.navigationController)!, showToast: true)
            case "Applikation Logout":
                businessLogicService.appLogOut(navigationController: (self.settingsViewController?.navigationController)!, showToast: true)
            default:
                print("an error occured")
            }
        }
    }
}

extension UIResponder {
    public var parentViewController: UIViewController? {
        return next as? UIViewController ?? next?.parentViewController
    }
}
