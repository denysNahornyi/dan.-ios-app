//
//  Dictionary.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 23.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

class Dictionary {
    static let log_out_successfull = "Abmeldung erfolgreich"
    static let password_changed = "Passwort geändert"
    
    static let old_password_label = "Altes"
    static let new_password_label = "Neues"
    static let repeat_password_label  = "Wiederholung"
    
    static let unreachable_server = "Server nicht erreichbar"
    static let server_error = "Serverfehler"
    static let access_denied = "Zugriff abgelehnt"
    static let not_identical_passwords = "Passwörter nicht identisch"
    
    static let dialogueText = "Sie können noch mal versuchen oder erneut versuchen, sich anzumelden"
    static let dialogueTextWhileNoConnection = "Es gibt kein Internet Verbindung"
    static let tryAgainButtoonText = "Noch mal versuchen"
    static let loginAgainButtonTetx = "Neues Anmeldung"
}
