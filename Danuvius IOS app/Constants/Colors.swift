//
//  Constants.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 20.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit

struct Colors {
    static let violetColor = UIColor(red: 0.20, green: 0.21, blue: 0.58, alpha: 1.00)
    static let blueColor = UIColor(red: 0.52, green: 0.76, blue: 0.90, alpha: 1.00)
}
