//
//  UrlPutViewController.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 24.06.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields

class UrlPutViewController: UIViewController {
    
    private var visualEffectsService = VisualEffectsService()
    private var businessLogicService = BusinessLogicService()
    
    @IBOutlet var mainViewUrl: MDCFilledTextField!
    @IBOutlet weak var backSquare: UIView!
    @IBOutlet weak var checkUrlButton: UIButton!
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func clickUrlButton(_ sender: Any) {
        dismissKeyboard()
        if mainViewUrl.text == "" {
            visualEffectsService.allertInput(input: self.mainViewUrl)
            return
        }
        businessLogicService.makeGetServerStatusOperations(url: mainViewUrl.text!, navigationController: navigationController!)
    }
    
    private func makeLoadOperations(){
        if (UserDefaults.standard.string(forKey: "applicationUrl") != nil) {
            mainViewUrl.text = UserDefaults.standard.string(forKey: "applicationUrl")
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            DispatchQueue.main.async {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "credentialsStoryBoard") as! CredentialsPutViewController
                self.navigationController?.pushViewController(viewController, animated: false)
            }
        } else {
            visualEffectsService.hideSpinner()
        }
    }
    
    private func changeDesign(){
        self.dismissKeyboard()
        visualEffectsService.setBackground(view: view, navigationController: navigationController!)
        backSquare.layer.cornerRadius = 20
        let inputText = mainViewUrl.text
        mainViewUrl.removeFromSuperview()
        mainViewUrl = MDCFilledTextField(frame: CGRect(x: mainViewUrl.frame.minX, y: mainViewUrl.frame.minY, width: backSquare.frame.width - 2*mainViewUrl.frame.minX, height: 10))
        mainViewUrl.text = inputText
        mainViewUrl.autocorrectionType = .no
        mainViewUrl.autocapitalizationType = .none
        mainViewUrl.setFilledBackgroundColor(.white, for: .normal)
        mainViewUrl.setFilledBackgroundColor(.white, for: .editing)
        mainViewUrl.setUnderlineColor(Colors.violetColor, for: .normal)
        mainViewUrl.setUnderlineColor(Colors.violetColor, for: .editing)
        mainViewUrl.label.text = "Url"
        mainViewUrl.setNormalLabelColor(.black, for: .normal)
        mainViewUrl.setNormalLabelColor(.black, for: .editing)
        mainViewUrl.setFloatingLabelColor(.black, for: .normal)
        mainViewUrl.setFloatingLabelColor(.black, for: .editing)
        mainViewUrl.setTextColor(.black, for: .normal)
        mainViewUrl.setTextColor(.black, for: .editing)
        mainViewUrl.placeholder = "https://"
        mainViewUrl.sizeToFit()
        print(mainViewUrl.frame)
        backSquare.addSubview(mainViewUrl)
        checkUrlButton.backgroundColor = Colors.blueColor
        mainViewUrl.keyboardType = .asciiCapable
        self.navigationItem.setHidesBackButton(true, animated: true)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        self.makeLoadOperations()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.setNeedsLayout()
        view.layoutIfNeeded()
        self.changeDesign()
    }
    
}
