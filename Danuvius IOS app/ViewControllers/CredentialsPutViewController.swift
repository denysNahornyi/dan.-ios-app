//
//  ViewController.swift
//  Danuvius IOS test app
//
//  Created by Denys on 25.05.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit
import WebKit
import Foundation
import MaterialComponents.MaterialTextControls_FilledTextFields

class CredentialsPutViewController: UIViewController {
    
    private var visualEffectsService = VisualEffectsService()
    private var businessLogicService = BusinessLogicService()

    @IBOutlet var usernameInput: MDCFilledTextField!
    @IBOutlet var passwordInput: MDCFilledTextField!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var backSquare: UIView!
    @IBOutlet weak var loginButton: UIButton!
    
    private var passwordIsSecure = true

    @IBAction func clickShowButton(_ sender: Any) {
        if passwordIsSecure {
            showButton.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            passwordInput.isSecureTextEntry = false
            passwordIsSecure = false
        } else {
            showButton.setImage(UIImage(systemName: "eye"), for: .normal)
            passwordInput.isSecureTextEntry = true
            passwordIsSecure = true
        }
    }
    
    @IBAction func backToUrlTapped(sender:UIButton){
        DispatchQueue.main.async {
            UserDefaults.standard.removeObject(forKey: "applicationUrl")
            UserDefaults.standard.removeObject(forKey: "applicationName")
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func clickLogin(_ sender: Any) {
        self.dismissKeyboard()
        if usernameInput.text == "" || passwordInput.text == ""{
            if usernameInput.text == "" {
                visualEffectsService.allertInput(input: self.usernameInput)
            }
            if passwordInput.text == "" {
                visualEffectsService.allertInput(input: self.passwordInput)
            }
            return
        }
        businessLogicService.makeGetRefreshJwtOperations(username: usernameInput.text!, password: passwordInput.text!, navigationController: self.navigationController!)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
        
    private func makeLoadOperations(){
        if UserDefaults.standard.string(forKey: "refreshJwt") != nil {
            if UserDefaults.standard.string(forKey: "accessJWT") != nil {
                businessLogicService.makeGetWebContetntOperations(refreshJwt: UserDefaults.standard.string(forKey: "refreshJwt")!,
                                                         accessJwt: UserDefaults.standard.string(forKey: "accessJWT")!,
                                                         navigationController: self.navigationController!)
            } else {
                businessLogicService.makeGetAccessJwtOperations(refreshJwt: UserDefaults.standard.string(forKey: "refreshJwt")!, navigationController: self.navigationController!)
            }
        } else {
            visualEffectsService.hideSpinner()
        }
    }
    
    private func changeDesign(){
        self.dismissKeyboard()
        loginButton.backgroundColor = Colors.blueColor
        self.title = UserDefaults.standard.string(forKey: "applicationName")
        visualEffectsService.setBackground(view: view, navigationController: navigationController!)
        backSquare.layer.cornerRadius = 20

        let button: UIButton = UIButton(type: UIButton.ButtonType.system)
        button.tintColor = UIColor.black
        button.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        button.setTitle(" URL", for: .normal)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 19)
        button.addTarget(self,action:#selector(backToUrlTapped), for:.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
       
        usernameInput.removeFromSuperview()
        passwordInput.removeFromSuperview()
        usernameInput = MDCFilledTextField(frame: CGRect(x: usernameInput.frame.minX, y: usernameInput.frame.minY, width: backSquare.frame.width - 2*usernameInput.frame.minX, height: 10))
        passwordInput = MDCFilledTextField(frame: CGRect(x: passwordInput.frame.minX, y: passwordInput.frame.minY, width: backSquare.frame.width - 2*passwordInput.frame.minX, height: 10))
        usernameInput.label.text = "Benutzername"
        passwordInput.label.text = "Password"
        passwordInput.rightView = showButton
        passwordInput.rightViewMode = .always
        passwordInput.isSecureTextEntry = true
        showButton.setImage(UIImage(systemName: "eye"), for: .normal)
        
        var inputs = [MDCFilledTextField]()
        inputs.append(usernameInput)
        inputs.append(passwordInput)
        for input in inputs {
            input.autocorrectionType = .no
            input.autocapitalizationType = .none
            input.setFilledBackgroundColor(.white, for: .normal)
            input.setFilledBackgroundColor(.white, for: .editing)
            input.setUnderlineColor(Colors.violetColor, for: .normal)
            input.setUnderlineColor(Colors.violetColor, for: .editing)
            input.keyboardType = .asciiCapable
            input.setFilledBackgroundColor(.white, for: .normal)
            input.setFilledBackgroundColor(.white, for: .editing)
            input.setUnderlineColor(Colors.violetColor, for: .normal)
            input.setUnderlineColor(Colors.violetColor, for: .editing)
            input.setNormalLabelColor(.black, for: .normal)
            input.setNormalLabelColor(.black, for: .editing)
            input.setFloatingLabelColor(.black, for: .normal)
            input.setFloatingLabelColor(.black, for: .editing)
            input.setTextColor(.black, for: .normal)
            input.setTextColor(.black, for: .editing)
            input.sizeToFit()
            backSquare.addSubview(input)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.makeLoadOperations()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setNeedsLayout()
        view.layoutIfNeeded()
        self.changeDesign()
    }
}

