//
//  WebViewController.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 24.06.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate,  WKNavigationDelegate {
    
    private var visualEffectsService = VisualEffectsService()
    private var timer = Timer.scheduledTimer(timeInterval: 1320.0, target: self, selector: #selector(timerAction),  userInfo: nil, repeats: false)
    
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    var phpSession: HTTPCookie?
    
    @IBAction func settingsTapped(sender:UIButton){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        DispatchQueue.main.async {
           let settingsViewController = storyBoard.instantiateViewController(withIdentifier: "settingsStoryBoard") as! SettingsViewController
           self.navigationController?.pushViewController(settingsViewController, animated: true)
        }
    }
    
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        if phpSession != nil {
            webConfiguration.websiteDataStore.httpCookieStore.setCookie(phpSession!)
        }
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if(timer.fireDate.timeIntervalSince(Date()) > 0) {
            timer = Timer.scheduledTimer(timeInterval: 1320.0, target: self, selector: #selector(timerAction),  userInfo: nil, repeats: false)
        } else {
             appDelegate?.lastUrl = navigationAction.request.url!
             self.navigationController!.popToRootViewController(animated: true)
             visualEffectsService.showSpinner()
             decisionHandler(.cancel)
             return
        }
        decisionHandler(.allow)
    }
    
    @objc func timerAction(){}
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        visualEffectsService.hideSpinner()
    }
    
    private func setupUI() {
        self.webView.allowsLinkPreview = false
        self.view.addSubview(self.webView)

        NSLayoutConstraint.activate([
            webView.topAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            webView.leftAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            webView.bottomAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            webView.rightAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
    
    private func loadUrl(){
        if appDelegate?.lastUrl != nil {
            self.webView.load(URLRequest(url: (appDelegate?.lastUrl)!))
        } else {
             self.webView.load(URLRequest(url: URL(string: UserDefaults.standard.string(forKey: "applicationUrl")!)!))
        }
    }
    
    private func changeDesign() {
        self.title = UserDefaults.standard.string(forKey: "applicationName")
        visualEffectsService.setBackground(view: view, navigationController: navigationController!)
        
        let button: UIButton = UIButton(type: UIButton.ButtonType.system)
        button.setImage(UIImage(systemName: "gear"), for: .normal)
        button.tintColor = UIColor.black
        button.addTarget(self,action:#selector(settingsTapped), for:.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        changeDesign()
        setupUI()
        loadUrl()
    }
    
}
