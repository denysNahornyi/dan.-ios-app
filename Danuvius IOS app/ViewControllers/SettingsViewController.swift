//
//  SettingsViewController.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 07.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    private var visualEffectsService = VisualEffectsService()
    
    private func changeDisign() {
        self.title = "Einstellungen"
        visualEffectsService.setBackground(view: view, navigationController: navigationController!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        changeDisign()
    }
}
