//
//  PasswordViewController.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 08.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields

class PasswordViewController: UIViewController {
    
    private var visualEffectsService = VisualEffectsService()
    private var businessLogicService = BusinessLogicService()
    
    @IBOutlet var oldPasswordInput: MDCFilledTextField!
    @IBOutlet var newPasswordInput: MDCFilledTextField!
    @IBOutlet var repeatNewPasswordInput: MDCFilledTextField!
    @IBOutlet weak var backSquare: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var showOldPasswordButton: UIButton!
    @IBOutlet weak var showNewPasswordButton: UIButton!
    @IBOutlet weak var showRepeatedPasswordButton: UIButton!
    
    private var oldPasswordIsSecure = true
    private var newPasswordIsSecure = true
    private var repeatedPasswordIsSecure = true
    
    @IBAction func clickShowOldPasswordButton(_ sender: Any) {
        if oldPasswordIsSecure {
            showOldPasswordButton.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            oldPasswordInput.isSecureTextEntry = false
            oldPasswordIsSecure = false
        } else {
            showOldPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
            oldPasswordInput.isSecureTextEntry = true
            oldPasswordIsSecure = true
        }
    }
    
    @IBAction func clickShowNewPasswordButton(_ sender: Any) {
        if newPasswordIsSecure {
            showNewPasswordButton.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            newPasswordInput.isSecureTextEntry = false
            newPasswordIsSecure = false
        } else {
            showNewPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
            newPasswordInput.isSecureTextEntry = true
            newPasswordIsSecure = true
        }
    }
    
    @IBAction func clickShowRepetedPasswordButton(_ sender: Any) {
        if repeatedPasswordIsSecure {
            showRepeatedPasswordButton.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            repeatNewPasswordInput.isSecureTextEntry = false
            repeatedPasswordIsSecure = false
        } else {
            showRepeatedPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
            repeatNewPasswordInput.isSecureTextEntry = true
            repeatedPasswordIsSecure = true
        }
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        dismissKeyboard()
        if oldPasswordInput.text == "" || newPasswordInput.text == "" || repeatNewPasswordInput.text == "" {
            if oldPasswordInput.text == "" {
                    visualEffectsService.allertInput(input: self.oldPasswordInput)
            }
            if newPasswordInput.text == "" {
                    visualEffectsService.allertInput(input: self.newPasswordInput)
            }
            if repeatNewPasswordInput.text == "" {
                    visualEffectsService.allertInput(input: self.repeatNewPasswordInput)
            }
            return
        } else if newPasswordInput.text != repeatNewPasswordInput.text {
            visualEffectsService.showToast(text: Dictionary.not_identical_passwords, color: "red", position: .bottom)
            visualEffectsService.allertInput(input: self.newPasswordInput)
            visualEffectsService.allertInput(input: self.repeatNewPasswordInput)
            return
        } else {
            businessLogicService.makePutNewPasswordOperations(oldPassword: oldPasswordInput.text!, newPassword: newPasswordInput.text!, view: self.view, navigationController: self.navigationController!)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func changeDesign(){
        self.dismissKeyboard()
        saveButton.backgroundColor = Colors.blueColor
        visualEffectsService.setBackground(view: view, navigationController: navigationController!)
        self.title = UserDefaults.standard.string(forKey: "applicationName")
        backSquare.layer.cornerRadius = 20
        
        oldPasswordInput.removeFromSuperview()
        newPasswordInput.removeFromSuperview()
        repeatNewPasswordInput.removeFromSuperview()
        oldPasswordInput = MDCFilledTextField(frame: CGRect(x: oldPasswordInput.frame.minX, y: oldPasswordInput.frame.minY, width: backSquare.frame.width - 2*oldPasswordInput.frame.minX, height: 10))
        newPasswordInput = MDCFilledTextField(frame: CGRect(x: newPasswordInput.frame.minX, y: newPasswordInput.frame.minY, width: backSquare.frame.width - 2*newPasswordInput.frame.minX, height: 10))
        repeatNewPasswordInput = MDCFilledTextField(frame: CGRect(x: repeatNewPasswordInput.frame.minX, y: repeatNewPasswordInput.frame.minY, width: backSquare.frame.width - 2*repeatNewPasswordInput.frame.minX, height: 10))
        oldPasswordInput.label.text = Dictionary.old_password_label
        newPasswordInput.label.text = Dictionary.new_password_label
        repeatNewPasswordInput.label.text = Dictionary.repeat_password_label
        showOldPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
        showNewPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
        showRepeatedPasswordButton.setImage(UIImage(systemName: "eye"), for: .normal)
        oldPasswordInput.rightView = showOldPasswordButton
        newPasswordInput.rightView = showNewPasswordButton
        repeatNewPasswordInput.rightView = showRepeatedPasswordButton
        
        var inputs = [MDCFilledTextField]()
        inputs.append(oldPasswordInput)
        inputs.append(newPasswordInput)
        inputs.append(repeatNewPasswordInput)
        
        for input in inputs {
            input.isSecureTextEntry = true
            input.keyboardType = .asciiCapable
            input.autocorrectionType = .no
            input.autocapitalizationType = .none
            input.setFilledBackgroundColor(.white, for: .normal)
            input.setFilledBackgroundColor(.white, for: .editing)
            input.setUnderlineColor(Colors.violetColor, for: .normal)
            input.setUnderlineColor(Colors.violetColor, for: .editing)
            input.setNormalLabelColor(.black, for: .normal)
            input.setNormalLabelColor(.black, for: .editing)
            input.setFloatingLabelColor(.black, for: .normal)
            input.setFloatingLabelColor(.black, for: .editing)
            input.setTextColor(.black, for: .normal)
            input.setTextColor(.black, for: .editing)
            input.sizeToFit()
            input.rightViewMode = .always
            backSquare.addSubview(input)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setNeedsLayout()
        view.layoutIfNeeded()
        self.changeDesign()
    }
}
