//
//  BusinessLogicService.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 23.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit

class BusinessLogicService {
    
    static private let restService = RestService()
    static private let visualEffectsService = VisualEffectsService()
    static private let helpService = HelpService()
    
    public func makeGetServerStatusOperations(url: String, navigationController: UINavigationController){
        let getServerStatusResponce = BusinessLogicService.restService.getServerStatus(urlInput: url)
        if getServerStatusResponce.0 == 200 && getServerStatusResponce.1 == "server is working" {
           let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
           DispatchQueue.main.async {
               let viewController = storyBoard.instantiateViewController(withIdentifier: "credentialsStoryBoard") as! CredentialsPutViewController
               navigationController.pushViewController(viewController, animated: true)
           }
        } else {
            BusinessLogicService.visualEffectsService.showToast(text: Dictionary.unreachable_server, color: "red", position: .bottom)
        }
    }
    
    public func makeGetRefreshJwtOperations(username: String, password: String, navigationController: UINavigationController){
        let getRefreshJWTResponce = BusinessLogicService.restService.getRefreshJWT(username: username, password: password, deviceId: (UIDevice.current.identifierForVendor?.uuidString)!)
        if getRefreshJWTResponce.0 == 200 && getRefreshJWTResponce.1 == "access confirmed" && getRefreshJWTResponce.2 != "" {
            UserDefaults.standard.set(getRefreshJWTResponce.2, forKey: "refreshJwt")
            UserDefaults.standard.set(username, forKey: "username")
            self.makeGetAccessJwtOperations(refreshJwt: getRefreshJWTResponce.2, navigationController: navigationController)
        } else {
            if getRefreshJWTResponce.0 == 500 && getRefreshJWTResponce.1 == "internal Server Error" {
                print("500 - internal server error")
            } else if getRefreshJWTResponce.0 == 401 && getRefreshJWTResponce.1 == "access denied" {
                BusinessLogicService.visualEffectsService.showToast(text: Dictionary.access_denied, color: "red", position: .bottom)
            } else if getRefreshJWTResponce.0 == 409 && getRefreshJWTResponce.1 == "username / password / IMEI was not found founded" {
                print("409 - field not found")
            } else {
                BusinessLogicService.visualEffectsService.showToast(text: Dictionary.server_error, color: "red", position: .bottom)
                print(String(getRefreshJWTResponce.0) + " - ERROR")
            }
        }
    }

    public func makeGetAccessJwtOperations(refreshJwt: String, navigationController: UINavigationController){
        let getAccessJWTResponce = BusinessLogicService.restService.getAccessJWT(refreshJWT: refreshJwt)
        if getAccessJWTResponce.0 == 200 && getAccessJWTResponce.1 == "access confirmed" && getAccessJWTResponce.2 != "" {
            self.makeGetWebContetntOperations(refreshJwt: refreshJwt, accessJwt: getAccessJWTResponce.2, navigationController: navigationController)
        } else {
            if getAccessJWTResponce.0 == 500 && getAccessJWTResponce.1 == "internal Server Error" {
                print("500 - internal server error")
            } else if getAccessJWTResponce.0 == 401 && getAccessJWTResponce.1 == "access denied" {
                BusinessLogicService.visualEffectsService.showToast(text: Dictionary.server_error, color: "red", position: .bottom)
            } else if getAccessJWTResponce.0 == 409 && getAccessJWTResponce.1 == "authorization header was not found" {
                print("409 - authorization header was not found")
            } else {
    //                self.showToast(text: "server error", color: "red", position: .bottom)
                BusinessLogicService.visualEffectsService.showDialogue(navigationController: navigationController)
                print(String(getAccessJWTResponce.0) + " - ERROR")
            }
            
        }
    }

    public func makeGetWebContetntOperations(refreshJwt: String, accessJwt: String, navigationController: UINavigationController){
        let getWebContentResponce = BusinessLogicService.restService.getWebContent(accessJWT: accessJwt)
        if(getWebContentResponce.0 == 200 && getWebContentResponce.1 == "login successful" ) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            DispatchQueue.main.async {
                let cookies = BusinessLogicService.self.helpService.readCookie(forURL: URL(string: UserDefaults.standard.string(forKey: "applicationUrl")!)!)
                let webViewController = storyBoard.instantiateViewController(withIdentifier: "webStoryBoard") as! WebViewController
                if cookies.count > 0 {
                    webViewController.phpSession = cookies[0]
                }
                BusinessLogicService.self.visualEffectsService.showSpinner()
                navigationController.pushViewController(webViewController, animated: false)
            }
        } else {
            if (getWebContentResponce.0 == 203 && getWebContentResponce.1 == "token expired") || (getWebContentResponce.0 == 401 && getWebContentResponce.1 == "no user with such access token")  {
                self.makeGetAccessJwtOperations(refreshJwt: refreshJwt, navigationController: navigationController)
            } else if getWebContentResponce.0 == 401 && getWebContentResponce.1 == "login denied" {
                print("401 - login denied")
            } else if getWebContentResponce.0 == 409 && getWebContentResponce.1 == "no appropriate jwt found" {
                print("409 - no appropriate jwt found")
            } else {
                BusinessLogicService.visualEffectsService.showToast(text: Dictionary.server_error, color: "red", position: .bottom)
                print(String(getWebContentResponce.0) + " - ERROR")
            }
        }
    }
    
    public func makePutNewPasswordOperations(oldPassword: String, newPassword: String, view: UIView, navigationController: UINavigationController){
        let newPasswordResponse = BusinessLogicService.restService.putNewPassword(oldPassword: oldPassword, newPassword: newPassword)
        if newPasswordResponse.0 == 200 && newPasswordResponse.1 == "password was changed" {
            BusinessLogicService.visualEffectsService.showToast(text: Dictionary.password_changed, color: "green", position: .center)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                _ = navigationController.popViewController(animated: true)
            }
        } else {
            if newPasswordResponse.0 == 203 && newPasswordResponse.1 == "incompatibility of old passwords" {
                BusinessLogicService.visualEffectsService.showToast(text: "incorrect old password", color: "red", position: .bottom)
                let passwordVieController = view.parentViewController as! PasswordViewController
                BusinessLogicService.visualEffectsService.allertInput(input: passwordVieController.oldPasswordInput)
            } else {
                if (newPasswordResponse.0 == 203 && newPasswordResponse.1 == "token expired") ||
                    (newPasswordResponse.0 == 401 && newPasswordResponse.1 == "no user with such access token") {
                    if (UserDefaults.standard.string(forKey: "applicationUrl") != nil) {
                        let getAccessJWTResponce = BusinessLogicService.restService.getAccessJWT(refreshJWT: UserDefaults.standard.string(forKey: "refreshJwt")!)
                        if getAccessJWTResponce.0 == 200 && getAccessJWTResponce.1 == "access confirmed" && getAccessJWTResponce.2 != "" {
                            self.makePutNewPasswordOperations(oldPassword: oldPassword, newPassword: newPassword, view: view, navigationController: navigationController)
                        } else if getAccessJWTResponce.0 == 401 && getAccessJWTResponce.1 == "access denied" {
                            self.userLogOut(navigationController: navigationController, showToast: false)
                        } else {
                            BusinessLogicService.visualEffectsService.showToast(text: "error, try later", color: "red", position: .bottom)
                        }
                    }
                } else {
                    BusinessLogicService.visualEffectsService.showToast(text: "error, try later", color: "red", position: .bottom)
                }
            }
        }
    }

    public func makeLogoutOperations(){
        BusinessLogicService.restService.logout()
    }
    
    public func userLogOut(navigationController: UINavigationController, showToast: Bool){
        self.makeLogoutOperations()
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.lastUrl = nil
        UserDefaults.standard.removeObject(forKey: "refreshJwt")
        UserDefaults.standard.removeObject(forKey: "accessJwt")
        navigationController.popToRootViewController(animated: true)
        BusinessLogicService.visualEffectsService.showSpinner()
        if showToast {
            BusinessLogicService.visualEffectsService.showToast(text: Dictionary.log_out_successfull, color: "green", position: .bottom)
        }
        }


    public func appLogOut(navigationController: UINavigationController, showToast: Bool){
        self.userLogOut(navigationController: navigationController, showToast: showToast)
        UserDefaults.standard.removeObject(forKey: "applicationUrl")
        UserDefaults.standard.removeObject(forKey: "applicationName")
    }
    
}
