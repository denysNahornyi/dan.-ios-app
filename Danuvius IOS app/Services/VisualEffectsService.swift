//
//  VisualEffectsService.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 23.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import Toast_Swift
import MaterialComponents.MaterialTextControls_FilledTextFields

class VisualEffectsService {
    
    public func showSpinner(){
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        var hasBlurEffect: Bool = false
        rootViewController.view.subviews.forEach({
            if $0 is UIVisualEffectView {
                hasBlurEffect = true
            }
        })
        if !hasBlurEffect {
            let backView = UIView(frame: rootViewController.view.bounds)
            backView.backgroundColor = Colors.violetColor
            rootViewController.view.addSubview(backView)
            
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = rootViewController.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.frame = rootViewController.view.bounds
            rootViewController.view.addSubview(blurEffectView)
            rootViewController.view.makeToastActivity(.center)
        }
    }
    
    public func hideSpinner(){
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        rootViewController.view.subviews.forEach({
            if $0 is UIVisualEffectView || $0.backgroundColor == Colors.violetColor {
                $0.removeFromSuperview()
            }
        })
        rootViewController.view.hideToastActivity()
    }
    
    public func setBackground(view: UIView, navigationController: UINavigationController){
        view.backgroundColor = Colors.violetColor
        navigationController.navigationBar.barTintColor = .white
    }
    
    public func allertInput(input: MDCFilledTextField){
        input.setUnderlineColor(.red, for: .normal)
        input.setUnderlineColor(.red, for: .editing)
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (timer) in
            input.setUnderlineColor(Colors.violetColor, for: .normal)
            input.setUnderlineColor(Colors.violetColor, for: .editing)
        }
    }
    
    public func showToast(text: String, color: String, position: ToastPosition){
        var style = ToastManager.shared.style
        style.verticalPadding = CGFloat(10)
        switch color {
        case "red":
            style.backgroundColor = UIColor(red: 0.9569, green: 0.2392, blue: 0, alpha: 1.0)
        case "green":
            style.backgroundColor = UIColor(red: 0.5373, green: 0.8471, blue: 0, alpha: 1.0)
        default:
            style.backgroundColor = UIColor(red: 0.7569, green: 0.7569, blue: 0.7569, alpha: 1.0)
        }
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        rootViewController.view.makeToast(text, duration: 1.7, position: position, style: style)
    }
    
    public func showConnectionError(){
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        let backView = UIView(frame: rootViewController.view.bounds)
        backView.backgroundColor = .black
        let symbol = UIImageView()
        symbol.image = UIImage(systemName: "wifi.slash")
        symbol.tintColor = .white
        symbol.contentMode = UIView.ContentMode.scaleAspectFit
        symbol.frame.size.width = 50
        symbol.frame.size.height = 50
        symbol.center = backView.center
        backView.addSubview(symbol)
        rootViewController.view.addSubview(backView)
    }
    
    public func hideConnectionError(){
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        rootViewController.view.subviews.forEach({
            if $0.backgroundColor == .black {
                $0.removeFromSuperview()
            }
        })
    }
    
    public func showDialogue(navigationController: UINavigationController){
        let rootViewController = UIApplication.shared.windows.first!.rootViewController as! UINavigationController
        var reachability: Reachability!
        do {
            try reachability = Reachability()
        } catch let error {
             print(error)
        }
        let alert : UIAlertController
        if reachability.connection != .unavailable{
            alert = UIAlertController(title: Dictionary.server_error, message: Dictionary.dialogueText, preferredStyle: UIAlertController.Style.alert)
            let loginAgainAction = UIAlertAction(title: Dictionary.loginAgainButtonTetx, style: .default) {
                UIAlertAction in
                BusinessLogicService().appLogOut(navigationController: navigationController, showToast: false)
            }
            alert.addAction(loginAgainAction)
        } else {
            alert = UIAlertController(title: Dictionary.server_error, message: Dictionary.dialogueTextWhileNoConnection, preferredStyle: UIAlertController.Style.alert)
        }
        let tryAgainAction = UIAlertAction(title: Dictionary.tryAgainButtoonText, style: .default) {
            UIAlertAction in
            BusinessLogicService().makeGetAccessJwtOperations(refreshJwt: UserDefaults.standard.string(forKey: "refreshJwt")!, navigationController: navigationController)
        }
        alert.addAction(tryAgainAction)
        
        rootViewController.present(alert, animated: true, completion: nil)
    }
    
    //    public func addTitle(navigationItem: UINavigationItem, text: String){
    //        let navView = UIView()
    //        let label = UILabel()
    //        label.text = text
    //        label.sizeToFit()
    //        label.center = navView.center
    //        label.textAlignment = NSTextAlignment.center
    //
    //        let image = UIImageView()
    //        image.image = UIImage(named: "logo fertig_ohne.png")
    //        let imageAspect = image.image!.size.width/image.image!.size.height
    //        // Setting the image frame so that it's immediately before the text:
    //        image.frame = CGRect(x: label.frame.size.height*imageAspect-label.frame.origin.x, y: label.frame.origin.y, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
    //        image.contentMode = UIView.ContentMode.scaleAspectFit
    //
    //        navView.addSubview(label)
    //        navView.addSubview(image)
    //
    //        navigationItem.titleView = navView
    //        navView.sizeToFit()
    //    }
    
    //    public func addLeftBarIcon(navigationItem: UINavigationItem) {
    //        let logoImage = UIImage.init(named: "logo fertig_ohne.png")
    //        let logoImageView = UIImageView.init(image: logoImage)
    //        logoImageView.frame = CGRect(x:0.0,y:0.0, width:80,height:35.0)
    //        logoImageView.contentMode = .scaleAspectFit
    //        let imageItem = UIBarButtonItem.init(customView: logoImageView)
    //        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 80)
    //        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 35)
    //        heightConstraint.isActive = true
    //        widthConstraint.isActive = true
    //        navigationItem.leftBarButtonItem =  imageItem
    //    }
    
}

