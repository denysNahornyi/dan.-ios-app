//
//  RestService.swift
//  Danuvius IOS app
//
//  Created by Denys Nahornyi on 23.07.20.
//  Copyright © 2020 Denys. All rights reserved.
//

import UIKit

class RestService {

    final private let getRefreshJwtUrl = "/backend/RESTapi/Controllers/AuthenticationRestController.php?view=getRefreshJWT"
    final private let getServerStatusUrl = "/backend/RESTapi/Controllers/ServerRestController.php?view=getServerStatus"
    final private let getAccessJwtUrl = "/backend/RESTapi/Controllers/AuthenticationRestController.php?view=getAccessJWT"
    final private let getWebContentUrl = "/backend/RESTapi/Controllers/WebViewRestController.php?view=getWebContent"
    final private let putNewPasswordUrl = "/backend/RESTapi/Controllers/AuthenticationRestController.php?view=putNewPassword"
    final private let logoutUrl = "/backend/RESTapi/Controllers/WebViewRestController.php?view=logout"
    
    static private let helpService = HelpService()
        
    public func getServerStatus(urlInput: String) -> (Int, String){
        let url = URL(string: urlInput + self.getServerStatusUrl)
        let sem = DispatchSemaphore(value: 0)
        var statusCode: Int = 500;
        var serverMessage: String = ""
        var applicationName: String = ""
        var sessionTimeOut: Int = 0
        guard let requestUrl = url else {
            return(statusCode, serverMessage)
        }
        var request = URLRequest(url: requestUrl)
        request.timeoutInterval = 2
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            if let error = error {
                print("Error took place while getting server \(error)")
                sem.signal()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
               let jsonObject: Data = (data)
               do {
                   let json = try JSONSerialization.jsonObject(with: jsonObject, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                   serverMessage = json["serverMessage"] as! String
                   applicationName = json["applicationName"] as! String
                   sessionTimeOut = json["timeout"] as! Int
                   UserDefaults.standard.set(urlInput, forKey: "applicationUrl")
                   UserDefaults.standard.set(applicationName, forKey: "applicationName")
                   UserDefaults.standard.set(sessionTimeOut, forKey: "sessionTimeOut")
               } catch let parsingError {
                    print("Parsing error", parsingError)
               }
            }
            sem.signal()
       }
       task.resume()
       sem.wait()
       return(statusCode, serverMessage)
    }
    
    func getRefreshJWT(username : String, password: String, deviceId: String) -> (Int, String, String){
        let url = URL(string: UserDefaults.standard.string(forKey: "applicationUrl")! + self.getRefreshJwtUrl)
        guard let requestUrl = url else { fatalError() }
        let encryptedPassword = RestService.helpService.MD5(string: password).map { String(format: "%02hhx", $0) }.joined()
        let json: [String: Any] = ["username" : username,
            "encryptedPassword": encryptedPassword,
            "imei": deviceId]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        var request = URLRequest(url: requestUrl)
        request.timeoutInterval = 2
        request.httpMethod = "POST"
            request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var statusCode: Int = 500;
        var refreshJwt: String = ""
        var serverMessage: String = ""
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let error = error {
                print("Error took place \(error)")
                sem.signal()
                return
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                let jsonObject: Data = (data)
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonObject, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    serverMessage = json["message"] as! String
                    if serverMessage == "access confirmed" {
                        refreshJwt = json["refreshJWT"] as! String
                    }
                } catch let parsingError {
                     print("Parsing error while getting refresh jwt", parsingError)
                }
            }
            sem.signal()
        }
        task.resume()
        sem.wait()
        return(statusCode, serverMessage, refreshJwt)
    }
    
    func getAccessJWT(refreshJWT: String) -> (Int, String, String){
        let url = URL(string: UserDefaults.standard.string(forKey: "applicationUrl")! + getAccessJwtUrl)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.timeoutInterval = 2
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(refreshJWT, forHTTPHeaderField: "AuthorizationToken")
        
        var statusCode: Int = 500;
        var serverMessage: String = ""
        var accessJwt: String = ""
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let error = error {
                print("Error took place \(error)")
                sem.signal()
                return
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                let jsonObject: Data = (data)
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonObject, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    serverMessage = json["message"] as! String
                    if serverMessage == "access confirmed" {
                        accessJwt = json["accessJWT"] as! String
                        UserDefaults.standard.set(accessJwt, forKey: "accessJwt")
                    }
                } catch let parsingError {
                     print("Parsing error while getting access jwt", parsingError)
                }
            }
            sem.signal()
        }
        task.resume()
        sem.wait()
        return(statusCode, serverMessage, accessJwt)
    }
    
    func getWebContent(accessJWT: String) -> (Int, String){
       let url = URL(string: UserDefaults.standard.string(forKey: "applicationUrl")! + getWebContentUrl)
       guard let requestUrl = url else { fatalError() }
       var request = URLRequest(url: requestUrl)
       request.timeoutInterval = 2
       request.httpMethod = "GET"
       request.addValue("application/json", forHTTPHeaderField: "Accept")
       request.addValue(accessJWT, forHTTPHeaderField: "AuthorizationToken")
       
       var statusCode: Int = 500;
       var serverMessage: String = ""
       
       let sem = DispatchSemaphore(value: 0)
       let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
           if let httpResponse = response as? HTTPURLResponse {
               statusCode = httpResponse.statusCode
           }
           if let error = error {
               print("Error took place \(error)")
               sem.signal()
               return
           }
           if let data = data, let _ = String(data: data, encoding: .utf8) {
               let jsonObject: Data = (data)
               do {
                   let json = try JSONSerialization.jsonObject(with: jsonObject, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                   serverMessage = json["serverMessage"] as! String
               } catch let parsingError {
                    print("Parsing error while getting web content", parsingError)
               }
           }
           sem.signal()
       }
       task.resume()
       sem.wait()
       return(statusCode, serverMessage)
    }
    
    public func putNewPassword(oldPassword: String, newPassword: String) -> (Int, String){
        let url = URL(string: UserDefaults.standard.string(forKey: "applicationUrl")! + putNewPasswordUrl)
        guard let requestUrl = url else { fatalError() }
        let encryptedCurrentPassword = RestService.helpService.MD5(string: oldPassword).map { String(format: "%02hhx", $0) }.joined()
        let encryptedNewPassword = RestService.helpService.MD5(string: newPassword).map { String(format: "%02hhx", $0) }.joined()
        let json: [String: Any] = ["oldPassword" : encryptedCurrentPassword, "newPassword": encryptedNewPassword]

        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        var request = URLRequest(url: requestUrl)
        request.timeoutInterval = 2
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(UserDefaults.standard.string(forKey: "accessJwt")!, forHTTPHeaderField: "AuthorizationToken")
        var statusCode: Int = 500;
        var serverMessage: String = ""
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let error = error {
                print("Error took place \(error)")
                sem.signal()
                return
            }
            if let data = data, let _ = String(data: data, encoding: .utf8) {
                let jsonObject: Data = (data)
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonObject, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    serverMessage = json["serverMessage"] as! String
                } catch let parsingError {
                     print("Parsing error while getting web content", parsingError)
                }
            }
            sem.signal()
        }
        task.resume()
        sem.wait()
        return(statusCode, serverMessage)
    }
    
    public func logout() -> (Int){
        let url = URL(string: UserDefaults.standard.string(forKey: "applicationUrl")! + logoutUrl)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.timeoutInterval = 2
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        var statusCode: Int = 500;
        let sem = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                statusCode = httpResponse.statusCode
            }
            if let error = error {
                print("Error took place \(error)")
                sem.signal()
                return
            }
            sem.signal()
        }
        task.resume()
        sem.wait()
        return(statusCode)
    }

}
